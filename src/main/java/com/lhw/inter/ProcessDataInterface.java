package com.lhw.inter;

import com.lhw.bean.CommonBean;
import org.apache.flink.streaming.api.datastream.DataStream;

/**
 * @ClassName ProcessDataInterface
 * @Author lihongwei
 * @Version 1.0.0
 * @Description Task的统一抽象接口
 * @Date 2023/5/5 11:19
 */
public interface ProcessDataInterface {
    void process(DataStream<CommonBean> commonData);
}
