package com.lhw.task;

import com.lhw.bean.CommonBean;
import com.lhw.config.CollectionConfig;
import com.lhw.inter.ProcessDataInterface;
import com.lhw.map.StudentMap;
import com.lhw.sink.SinkMysql;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

/**
 * @ClassName StudentTask
 * @Author lihongwei
 * @Version 1.0.0
 * @Description 技术可行性测试代码
 * @Date 2023/5/5 11:14
 */
public class StudentTask implements ProcessDataInterface {
    public void process(DataStream<CommonBean> commonData) {
        // 数据过滤，只保留student表的数据
        SingleOutputStreamOperator<CommonBean> filterDataStream = commonData.filter(
                new FilterFunction<CommonBean>() {
                    public boolean filter(CommonBean commonBean) throws Exception {
                        return commonBean.getSource().get("table").equals("student");
                    }
                }
        );
//        filterDataStream.print();
        // 新建侧边流分支（删除）
        //封装删除
        final OutputTag<CommonBean> deleteOpt = new OutputTag<CommonBean>("deleteOpt", TypeInformation.of(CommonBean.class));
        //数据分流
        SingleOutputStreamOperator<CommonBean> processData = filterDataStream.process(
                new ProcessFunction<CommonBean, CommonBean>() {
                    @Override
                    public void processElement(CommonBean commonBean, Context context, Collector<CommonBean> collector) throws Exception {
                        if (commonBean.getOp().equals("c") || commonBean.getOp().equals("u") || commonBean.getOp().equals("r")) {
                            //insert or update
                            collector.collect(commonBean);
                        }else {
                            //delete
                            context.output(deleteOpt, commonBean);
                        }
                    }
                }
        );

        String replaceSql = "replace into %s value(?,?,?,?,?)";
        String deleteSql = "delete from %s where id=?";

        //insert,update
        SingleOutputStreamOperator<Row> mapDS = processData.map(new StudentMap());
        mapDS.addSink(new SinkMysql(String.format(replaceSql, CollectionConfig.config.getProperty("mysql.student.sql.table"))));
        mapDS.print("=========");
//        //delete
//        SingleOutputStreamOperator<Row> rowSingleOutputStreamOperator = processData.getSideOutput(deleteOpt).map(new StudentMap());
//        rowSingleOutputStreamOperator.addSink(new SinkMysql(String.format(deleteSql,CollectionConfig.config.getProperty("mysql.student.sql.table"))));
//        rowSingleOutputStreamOperator.print("########");
    }
}
