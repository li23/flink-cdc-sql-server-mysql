package com.lhw.bean;

import lombok.Data;

import java.util.Map;

/**
 * @ClassName CommonBean
 * @Author lihongwei
 * @Version 1.0.0
 * @Description 技术可行性验证，通用类
 * @Date 2023/5/5 10:43
 */
@Data
public class CommonBean {
    private Map<String,String> before;
    private Map<String,String> after;
    private Map<String,String> source;
    private String op;
    private String ts_ms;
    private String transaction;
}
