package com.lhw.map;

import com.lhw.bean.CommonBean;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.types.Row;
import java.sql.Timestamp;

/**
 * @ClassName StudentMap
 * @Author lihongwei
 * @Version 1.0.0
 * @Description 技术可行性验证代码，将通用类转换为与业务对应的Row
 * @Date 2023/5/5 11:47
 */
public class StudentMap extends RichMapFunction<CommonBean, Row> {
    public Row map(CommonBean commonBean) {
        Row row;
        if (commonBean.getOp().equals("c") || commonBean.getOp().equals("u")|| commonBean.getOp().equals("r")) {
            row = new Row(5);
            row.setField(0, Integer.parseInt(commonBean.getAfter().get("id")));//int
            row.setField(1, commonBean.getAfter().get("name"));//varchar
            row.setField(2, Double.valueOf(commonBean.getAfter().get("age")));//decimal
            row.setField(3, Long.valueOf(commonBean.getAfter().get("mark")));//bigint
            row.setField(4, new Timestamp(Long.valueOf(commonBean.getAfter().get("create_time"))));//datetime
            return row;
        }else{
            row = new Row(1);
            row.setField(0,  Integer.parseInt(commonBean.getAfter().get("id")));
            return row;
        }

    }

//    public static void main(String[] args) {
//        Timestamp timestamp = new Timestamp(Long.valueOf("1682294400000"));
//        System.out.println(timestamp.toString());
//    }
}
