package com.lhw.map;

import com.alibaba.fastjson.JSON;
import com.lhw.bean.CommonBean;
import org.apache.flink.api.common.functions.MapFunction;

/**
 * @ClassName CommonBeanMap
 * @Author lihongwei
 * @Version 1.0.0
 * @Description 技术可行性验证代码，将从SqlServer监听到的数据转换为统一的类型
 * @Date 2023/5/5 10:48
 */
public class CommonBeanMap implements MapFunction<String, CommonBean> {
    public CommonBean map(String s) throws Exception {
        CommonBean commonBean = JSON.parseObject(s, CommonBean.class);
        return commonBean;
    }
}
