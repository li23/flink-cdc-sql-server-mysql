package com.lhw.map;

import com.lhw.bean.CommonBean;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.types.Row;
import java.sql.Timestamp;

/**
 * @ClassName StudentMap
 * @Author lihongwei
 * @Version 1.0.0
 * @Description 技术可行性验证代码，将通用类转换为与业务对应的Row
 * @Date 2023/5/5 11:47
 */
public class TBoaContractDocMap extends RichMapFunction<CommonBean, Row> {
    /**
     *  FID bigint NOT NULL,
     * 	FBILLNO nvarchar(70) NOT NULL,
     * 	FDOCUMENTSTATUS char(1) NOT NULL,
     * 	F_BOA_CREATEDATE datetime NULL,
     * 	F_BOA_MODIFIERID int NOT NULL,
     * 	F_BOA_CREATORID int NOT NULL,
     * 	F_BOA_MODIFYDATE datetime NULL,
     * 	F_BOA_APPROVEDATE datetime NULL,
     * 	F_BOA_APPROVERID int NOT NULL,
     * 	F_BOA_ORGID int NOT NULL,
     * 	F_BOA_USEORGID int NOT NULL,
     * 	F_BOA_DATE datetime NULL,
     * 	F_BOA_PROJECTNAME nvarchar(2000) NOT NULL,
     * 	F_BOA_PROJECTADDRESS nvarchar(50) NOT NULL,
     * 	F_BOA_GROUP int NOT NULL,
     * 	F_BOA_ACHARGE nvarchar(50) NOT NULL,
     * 	F_BOA_REMARKS nvarchar(255) NOT NULL,
     * 	F_BOA_BUSINESSMODEL varchar(20) NOT NULL,
     * 	F_BOA_CUSTID int NOT NULL,
     * 	F_BOA_WEBSITE nvarchar(50) NOT NULL,
     * 	F_BOA_CITY int NOT NULL,
     * 	F_BOA_COUNT nvarchar(50) NOT NULL,
     * 	F_BOA_SIGNTEL nvarchar(50) NOT NULL,
     * 	F_BOA_CHECKTEL nvarchar(50) NOT NULL,
     * 	F_BOA_RECEIVETEL nvarchar(50) NOT NULL,
     * 	F_BOA_TYPE varchar(20) NOT NULL,
     * 	F_BOA_TOWN int NOT NULL,
     * 	F_BOA_CAPITALSTATUS char(1) NOT NULL,
     * 	F_BOA_CAPITALDATE datetime NULL,
     * 	F_BOA_PROJECTSTATUS char(1) NOT NULL,
     * 	F_BOA_PROJECTDATE datetime NULL,
     * 	F_BOA_SIGNSTATUS char(1) NOT NULL,
     * 	F_BOA_SIGNDATE datetime NULL,
     * 	F_BOA_ISLETTER char(1) NOT NULL,
     * 	F_BOA_PAYDATE datetime NULL,
     * 	F_BOA_RATE decimal(23, 10) NOT NULL,
     * 	F_BOA_CONTRACTQTY decimal(23, 10) NOT NULL,
     * 	F_BOA_AMOUNT decimal(23, 10) NOT NULL,
     * 	F_BOA_RECAMOUNT decimal(23, 10) NOT NULL,
     * 	F_BOA_REMARK nvarchar(50) NOT NULL,
     * 	F_BOA_ISDETAIL char(1) NOT NULL,
     * 	F_BOA_SOURCEBILLNO nvarchar(50) NOT NULL,
     * 	F_BOA_SRCBILLID nvarchar(50) NOT NULL,
     * 	F_BOA_BILLTYPEID varchar(36) NOT NULL,
     * 	F_BOA_SALEID int NOT NULL,
     * 	F_BOA_SALEORGID int NOT NULL,
     * 	F_BOA_PURQTY decimal(23, 10) NOT NULL,
     * 	F_BOA_PICKQTY decimal(23, 10) NOT NULL,
     * 	F_BOA_BIDDQTY decimal(23, 10) NOT NULL,
     * 	F_BOA_BIDDAMOUNT decimal(23, 10) NOT NULL,
     * 	F_BOA_BONDPAYAMT decimal(23, 10) NOT NULL,
     * 	F_BOA_OWNER nvarchar(50) NOT NULL,
     * 	F_BOA_CHARGER int NOT NULL,
     * 	F_BOA_ACCOUNTDAY int NOT NULL,
     * 	F_BOA_ACCMETHOD varchar(20) NOT NULL,
     * 	F_BOA_DAYS varchar(20) NOT NULL,
     * 	F_BOA_PURAMOUNT decimal(23, 10) NOT NULL,
     * 	F_BOA_COMSTATUS varchar(20) NOT NULL,
     * 	F_BOA_ISCHANGED char(1) NOT NULL,
     * 	F_BOA_REMBIDDQTY decimal(23, 10) NOT NULL,
     * 	F_BOA_BUYER int NOT NULL,
     * 	F_BOA_DOCUMENTCLERK int NOT NULL,
     * 	F_BOA_PERBOND decimal(23, 10) NOT NULL,
     * 	F_BOA_OWNERINFORMATION nvarchar(50) NOT NULL,
     * 	F_BOA_ONLINEPRICE decimal(23, 10) NOT NULL,
     * 	F_BOA_REFERREMARKS nvarchar(100) NOT NULL,
     * 	F_BOA_FLOATINGRATE decimal(23, 10) NOT NULL,
     * 	F_BOA_CONSUBDATE datetime NULL,
     * 	F_BOA_DOUSIGNEDDATE datetime NULL,
     * 	F_BOA_BUSINESS int NOT NULL,
     * 	F_BOA_REFER varchar(20) NOT NULL,
     * 	F_BOA_PARTYB nvarchar(36) NOT NULL,
     * 	F_BOA_SECPARTY bigint NOT NULL,
     * 	F_BOA_OVERDUEAMOUNT decimal(23, 10) NOT NULL,
     * 	F_BOA_OVERDUEDAYS decimal(23, 10) NOT NULL,
     * 	F_BOA_OVERMAXAMOUNT decimal(23, 10) NOT NULL)
     * @param commonBean
     * @return
     * @throws Exception
     */
    public Row map(CommonBean commonBean) {
        Row row;
        if (commonBean.getOp().equals("c") || commonBean.getOp().equals("u")|| commonBean.getOp().equals("r")) {
            row = new Row(76);
            row.setField(0, Long.valueOf(commonBean.getAfter().get("FID")));//bigint
            row.setField(1, commonBean.getAfter().get("FBILLNO"));//nvarchar
            row.setField(2, commonBean.getAfter().get("FDOCUMENTSTATUS"));//char
            row.setField(3, commonBean.getAfter().get("F_BOA_CREATEDATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_CREATEDATE"))));//datetime
            row.setField(4, Integer.parseInt(commonBean.getAfter().get("F_BOA_MODIFIERID")));//int
            row.setField(5, Integer.parseInt(commonBean.getAfter().get("F_BOA_CREATORID")));//int
            row.setField(6, commonBean.getAfter().get("F_BOA_MODIFYDATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_MODIFYDATE"))));//datetime
            row.setField(7, commonBean.getAfter().get("F_BOA_APPROVEDATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_APPROVEDATE"))));//datetime
            row.setField(8, Integer.parseInt(commonBean.getAfter().get("F_BOA_APPROVERID")));//int
            row.setField(9, Integer.parseInt(commonBean.getAfter().get("F_BOA_ORGID")));//int
            row.setField(10, Integer.parseInt(commonBean.getAfter().get("F_BOA_USEORGID")));//int
            row.setField(11, commonBean.getAfter().get("F_BOA_DATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_DATE"))));//datetime
            row.setField(12, commonBean.getAfter().get("F_BOA_PROJECTNAME"));//nvarchar
            row.setField(13, commonBean.getAfter().get("F_BOA_PROJECTADDRESS"));//nvarchar
            row.setField(14, Integer.parseInt(commonBean.getAfter().get("F_BOA_GROUP")));//int
            row.setField(15, commonBean.getAfter().get("F_BOA_ACHARGE"));//nvarchar
            row.setField(16, commonBean.getAfter().get("F_BOA_REMARKS"));//nvarchar
            row.setField(17, commonBean.getAfter().get("F_BOA_BUSINESSMODEL"));//varchar
            row.setField(18, Integer.parseInt(commonBean.getAfter().get("F_BOA_CUSTID")));//int
            row.setField(19, commonBean.getAfter().get("F_BOA_WEBSITE"));//nvarchar
            row.setField(20, Integer.parseInt(commonBean.getAfter().get("F_BOA_CITY")));//int
            row.setField(21, commonBean.getAfter().get("F_BOA_COUNT"));//nvarchar
            row.setField(22, commonBean.getAfter().get("F_BOA_SIGNTEL"));//nvarchar
            row.setField(23, commonBean.getAfter().get("F_BOA_CHECKTEL"));//nvarchar
            row.setField(24, commonBean.getAfter().get("F_BOA_RECEIVETEL"));//nvarchar
            row.setField(25, commonBean.getAfter().get("F_BOA_TYPE"));//varchar
            row.setField(26, Integer.parseInt(commonBean.getAfter().get("F_BOA_TOWN")));//int
            row.setField(27, commonBean.getAfter().get("F_BOA_CAPITALSTATUS"));//char
            row.setField(28, commonBean.getAfter().get("F_BOA_CAPITALDATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_CAPITALDATE"))));//datetime
            row.setField(29, commonBean.getAfter().get("F_BOA_PROJECTSTATUS"));//char
            row.setField(30, commonBean.getAfter().get("F_BOA_PROJECTDATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_PROJECTDATE"))));//datetime
            row.setField(31, commonBean.getAfter().get("F_BOA_SIGNSTATUS"));//char
            row.setField(32, commonBean.getAfter().get("F_BOA_SIGNDATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_SIGNDATE"))));//datetime
            row.setField(33, commonBean.getAfter().get("F_BOA_ISLETTER"));//char
            row.setField(34, commonBean.getAfter().get("F_BOA_PAYDATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_PAYDATE"))));//datetime
            row.setField(35, Double.valueOf(commonBean.getAfter().get("F_BOA_RATE")));//decimal(23, 10)
            row.setField(36, Double.valueOf(commonBean.getAfter().get("F_BOA_CONTRACTQTY")));//decimal
            row.setField(37, Double.valueOf(commonBean.getAfter().get("F_BOA_AMOUNT")));//decimal
            row.setField(38, Double.valueOf(commonBean.getAfter().get("F_BOA_RECAMOUNT")));//decimal
            row.setField(39, commonBean.getAfter().get("F_BOA_REMARK"));//nvarchar
            row.setField(40, commonBean.getAfter().get("F_BOA_ISDETAIL"));//char
            row.setField(41, commonBean.getAfter().get("F_BOA_SOURCEBILLNO"));//nvarchar
            row.setField(42, commonBean.getAfter().get("F_BOA_SRCBILLID"));//nvarchar
            row.setField(43, commonBean.getAfter().get("F_BOA_BILLTYPEID"));//varchar
            row.setField(44, Integer.parseInt(commonBean.getAfter().get("F_BOA_SALEID")));//int
            row.setField(45, Integer.parseInt(commonBean.getAfter().get("F_BOA_SALEORGID")));//int
            row.setField(46, Double.valueOf(commonBean.getAfter().get("F_BOA_PURQTY")));//decimal
            row.setField(47, Double.valueOf(commonBean.getAfter().get("F_BOA_PICKQTY")));//decimal
            row.setField(48, Double.valueOf(commonBean.getAfter().get("F_BOA_BIDDQTY")));//decimal
            row.setField(49, Double.valueOf(commonBean.getAfter().get("F_BOA_BIDDAMOUNT")));//decimal
            row.setField(50, Double.valueOf(commonBean.getAfter().get("F_BOA_BONDPAYAMT")));//decimal
            row.setField(51, commonBean.getAfter().get("F_BOA_OWNER"));//nvarchar
            row.setField(52, Integer.parseInt(commonBean.getAfter().get("F_BOA_CHARGER")));//int
            row.setField(53, Integer.parseInt(commonBean.getAfter().get("F_BOA_ACCOUNTDAY")));//int
            row.setField(54, commonBean.getAfter().get("F_BOA_ACCMETHOD"));//varchar
            row.setField(55, commonBean.getAfter().get("F_BOA_DAYS"));//varchar
            row.setField(56, Double.valueOf(commonBean.getAfter().get("F_BOA_PURAMOUNT")));//decimal
            row.setField(57, commonBean.getAfter().get("F_BOA_COMSTATUS"));//varchar
            row.setField(58, commonBean.getAfter().get("F_BOA_ISCHANGED"));//char
            row.setField(59, Double.valueOf(commonBean.getAfter().get("F_BOA_REMBIDDQTY")));//decimal
            row.setField(60, Integer.parseInt(commonBean.getAfter().get("F_BOA_BUYER")));//int
            row.setField(61, Integer.parseInt(commonBean.getAfter().get("F_BOA_DOCUMENTCLERK")));//int
            row.setField(62, Double.valueOf(commonBean.getAfter().get("F_BOA_PERBOND")));//decimal
            row.setField(63, commonBean.getAfter().get("F_BOA_OWNERINFORMATION"));//nvarchar
            row.setField(64, Double.valueOf(commonBean.getAfter().get("F_BOA_ONLINEPRICE")));//decimal
            row.setField(65, commonBean.getAfter().get("F_BOA_REFERREMARKS"));//nvarchar
            row.setField(66, Double.valueOf(commonBean.getAfter().get("F_BOA_FLOATINGRATE")));//decimal
            row.setField(67, commonBean.getAfter().get("F_BOA_CONSUBDATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_CONSUBDATE"))));//datetime
            row.setField(68, commonBean.getAfter().get("F_BOA_DOUSIGNEDDATE")==null?null:new Timestamp(Long.valueOf(commonBean.getAfter().get("F_BOA_DOUSIGNEDDATE"))));//datetime
            row.setField(69, Integer.parseInt(commonBean.getAfter().get("F_BOA_BUSINESS")));//int
            row.setField(70, commonBean.getAfter().get("F_BOA_REFER"));//varchar
            row.setField(71, commonBean.getAfter().get("F_BOA_PARTYB"));//nvarchar
            row.setField(72, Long.valueOf(commonBean.getAfter().get("F_BOA_SECPARTY")));//bigint
            row.setField(73, Double.valueOf(commonBean.getAfter().get("F_BOA_OVERDUEAMOUNT")));//decimal
            row.setField(74, Double.valueOf(commonBean.getAfter().get("F_BOA_OVERDUEDAYS")));//decimal
            row.setField(75, Double.valueOf(commonBean.getAfter().get("F_BOA_OVERMAXAMOUNT")));//decimal
        }else{
            row = new Row(1);
            row.setField(0, Long.valueOf(commonBean.getAfter().get("FID")));//bigint
        }
        return row;
    }
}
