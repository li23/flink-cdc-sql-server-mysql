package com.lhw.config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * @ClassName CollectionConfig
 * @Author lihongwei
 * @Version 1.0.0
 * @Description 配置文件加载
 * @Date 2023/5/5 9:58
 */


public class CollectionConfig {
    private static final Logger LOG = LoggerFactory.getLogger(CollectionConfig.class);
    public final static Properties config = new Properties();
    static {
        InputStream profile = CollectionConfig.class.getClassLoader().getResourceAsStream("config.properties");
        try {
            config.load(profile);
        } catch (IOException e) {
            LOG.info("load profile error!");
        }
        for (Map.Entry<Object, Object> kv : config.entrySet()) {
            LOG.info(kv.getKey()+"="+kv.getValue());
        }
    }
//
//    public static void main(String[] args) {
//        String property = config.getProperty("mysql.driver");
//        System.out.println("------------------"+property);
//    }
}